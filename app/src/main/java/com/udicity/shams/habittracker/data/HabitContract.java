package com.udicity.shams.habittracker.data;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by Shams_Keshk on 05/09/17.
 */

public final class HabitContract {

    public static final String CONTENT_AUTHORITY = "com.udicity.shams.habittracker";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    public static final String PATH_HABITS = "habits";

    private HabitContract() {

    }

    public static final class HabitEntry implements BaseColumns {

        public static final String CONTENT_LIST_TYPE =
                ContentResolver.CURSOR_DIR_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_HABITS ;

        public static final String CONTENT_ITEM_TYPE =
                ContentResolver.CURSOR_ITEM_BASE_TYPE + "/" + CONTENT_AUTHORITY + "/" + PATH_HABITS ;

        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI , PATH_HABITS);

        public final static String TABLE_NAME = "habits";

        public final static String _ID = BaseColumns._ID;

        public final static String COLUMN_HABIT_NAME = "name";

        public final static String COLUMN_HABIT_TIMING = "timing";

        public final static String COLUMN_HABIT_day = "day";

        //Timing Constant Values
        public final static int TIMING_UNKNOWN = 0;

        public final static int TIMING_AM = 1;

        public final static int TIMING_PM = 2;

        //Days Constant Values
        public final static String DAY_SATURDAY = "Saturday";

        public final static String DAY_SUNDAY = "Sunday";

        public final static String DAY_MONDAY = "Monday";

        public final static String DAY_TUESDAY = "Tuesday";

        public final static String DAY_WEDNESDAY = "Wednesday";

        public final static String DAY_THURSDAY = "Thursday";

        public final static String DAY_FRIDAY = "Friday";

    }
}