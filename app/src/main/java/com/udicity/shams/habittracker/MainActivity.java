package com.udicity.shams.habittracker;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.udicity.shams.habittracker.data.HabitContract.HabitEntry;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.fab)
    FloatingActionButton floatingActionButton;

    @BindView(R.id.habit_information_text_view)
    TextView habitInfoTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EditorActivity.class);
                startActivity(intent);
            }
        });

        displayHabitsInfo();
    }

    private Cursor readFromDataBase() {

        String[] projection = {HabitEntry._ID,
                HabitEntry.COLUMN_HABIT_NAME,
                HabitEntry.COLUMN_HABIT_TIMING,
                HabitEntry.COLUMN_HABIT_day};

        Cursor cursor = getContentResolver().query(HabitEntry.CONTENT_URI,projection,null,null,null);

        return cursor;
    }

    private void displayHabitsInfo() {

        Cursor cursor = readFromDataBase();

        int idColumnIndex = cursor.getColumnIndex(HabitEntry._ID);
        int nameColumnIndex = cursor.getColumnIndex(HabitEntry.COLUMN_HABIT_NAME);
        int timingColumnIndex = cursor.getColumnIndex(HabitEntry.COLUMN_HABIT_TIMING);
        int dayColumnIndex = cursor.getColumnIndex(HabitEntry.COLUMN_HABIT_day);

        try {

            habitInfoTextView.setText(getString(R.string.habits));

            while (cursor.moveToNext()) {
                int id = cursor.getInt(idColumnIndex);
                String habitName = cursor.getString(nameColumnIndex);

                String habitTiming;
                switch (cursor.getInt(timingColumnIndex)) {
                    case 1:
                        habitTiming = getString(R.string.timing_am);
                        break;
                    case 2:
                        habitTiming = getString(R.string.timing_pm);
                        break;
                    default:
                        habitTiming = getString(R.string.timing_unknown);
                        break;
                }

                String habitDay = cursor.getString(dayColumnIndex);

                habitInfoTextView.append("\n\n" + id + "\t" + " | " + habitName + "\t\t" + habitDay + "\t\t" + habitTiming);
            }

        } finally {
            cursor.close();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        displayHabitsInfo();
    }
}
