package com.udicity.shams.habittracker;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.udicity.shams.habittracker.data.HabitContract.HabitEntry;
import com.udicity.shams.habittracker.data.HabitDbHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EditorActivity extends AppCompatActivity {

    @BindView(R.id.timing_spinner_id)
    Spinner timingSpinner;

    @BindView(R.id.day_spinner_id)
    Spinner daySpinner;

    @BindView(R.id.habit_name_edit_text_id)
    EditText habitNameView;

    private int habitTiming = 0;
    private String habitDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editor);
        ButterKnife.bind(this);

        setUpTimingSpinner();
        setUpDaySpinner();
    }

    private void insertHabit() {
        String habitName = habitNameView.getText().toString().trim();
        if (!TextUtils.isEmpty(habitName)) {

            ContentValues values = new ContentValues();
            values.put(HabitEntry.COLUMN_HABIT_NAME, habitName);
            values.put(HabitEntry.COLUMN_HABIT_TIMING, habitTiming);
            values.put(HabitEntry.COLUMN_HABIT_day, habitDay);

            Uri uri = getContentResolver().insert(HabitEntry.CONTENT_URI,values);

            /*
            long habitId = sqLiteDatabase.insert(HabitEntry.TABLE_NAME, null, values);
            if (habitId == -1) {
                Toast.makeText(getApplicationContext(), getString(R.string.error_while_saving_your_habit), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), habitName + getString(R.string.saved_with_id) + habitId, Toast.LENGTH_SHORT).show();
            }

        } else {
            Toast.makeText(getApplicationContext(), getString(R.string.habit_can_not_be_null), Toast.LENGTH_SHORT).show();
        }
        */
    }
    }

    private void setUpDaySpinner() {
        ArrayAdapter dayArrayAdapter = ArrayAdapter.createFromResource(this, R.array.array_day_list,
                android.R.layout.simple_spinner_item);
        dayArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        daySpinner.setAdapter(dayArrayAdapter);
        daySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.day_saturday))) {
                        habitDay = HabitEntry.DAY_SATURDAY;
                    } else if (selection.equals(getString(R.string.day_sunday))) {
                        habitDay = HabitEntry.DAY_SUNDAY;
                    } else if (selection.equals(getString(R.string.day_monday))) {
                        habitDay = HabitEntry.DAY_MONDAY;
                    } else if (selection.equals(getString(R.string.day_tuesday))) {
                        habitDay = HabitEntry.DAY_TUESDAY;
                    } else if (selection.equals(getString(R.string.day_wednesday))) {
                        habitDay = HabitEntry.DAY_WEDNESDAY;
                    } else if (selection.equals(getString(R.string.day_thursday))) {
                        habitDay = HabitEntry.DAY_THURSDAY;
                    } else {
                        habitDay = HabitEntry.DAY_FRIDAY;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                habitDay = HabitEntry.DAY_FRIDAY;
            }
        });
    }

    private void setUpTimingSpinner() {
        ArrayAdapter timingArrayAdapter = ArrayAdapter.createFromResource(this, R.array.array_timing_list,
                android.R.layout.simple_spinner_item);
        timingArrayAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);

        timingSpinner.setAdapter(timingArrayAdapter);

        timingSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if (!TextUtils.isEmpty(selection)) {
                    if (selection.equals(getString(R.string.timing_am))) {
                        habitTiming = HabitEntry.TIMING_AM;
                    } else if (selection.equals(getString(R.string.timing_pm))) {
                        habitTiming = HabitEntry.TIMING_PM;
                    } else {
                        habitTiming = HabitEntry.TIMING_UNKNOWN;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                habitTiming = HabitEntry.TIMING_UNKNOWN;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_editor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.save_habit_item:
                insertHabit();
                finish();
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
