package com.udicity.shams.habittracker.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.udicity.shams.habittracker.data.HabitContract.HabitEntry;

/**
 * Created by Shams_Keshk on 05/09/17.
 */

public class HabitDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "habit.db";

    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " + HabitEntry.TABLE_NAME + " ( "
            + HabitEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT , "
            + HabitEntry.COLUMN_HABIT_NAME + " TEXT NOT NULL , "
            + HabitEntry.COLUMN_HABIT_TIMING + " INTEGER NOT NULL DEFAULT 0 ,"
            + HabitEntry.COLUMN_HABIT_day + " TEXT NOT NULL " + ");";
    private static final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " + HabitEntry.TABLE_NAME;

    public HabitDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}