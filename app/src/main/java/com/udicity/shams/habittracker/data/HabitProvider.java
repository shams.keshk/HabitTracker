package com.udicity.shams.habittracker.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.udicity.shams.habittracker.data.HabitContract.HabitEntry;

import static android.R.attr.id;


/**
 * Created by Shams_Keshk on 09/09/17.
 */

public class HabitProvider extends ContentProvider {

    private static final String LOG_TAG = HabitProvider.class.getSimpleName();

    private HabitDbHelper habitDbHelper;

    private static final int HABITS = 100;

    private static final int HABIT_ID = 101 ;

    private static final UriMatcher uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        uriMatcher.addURI(HabitContract.CONTENT_AUTHORITY,HabitContract.PATH_HABITS,HABITS);
        uriMatcher.addURI(HabitContract.CONTENT_AUTHORITY,HabitContract.PATH_HABITS+"/#" ,HABIT_ID);
    }

    @Override
    public boolean onCreate() {
        habitDbHelper = new HabitDbHelper(getContext());

        return true;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection
            , @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteDatabase sqLiteDatabase = habitDbHelper.getReadableDatabase();

        Cursor cursor;

        int match = uriMatcher.match(uri);
        switch (match){
            case HABITS :
                cursor = sqLiteDatabase.query(HabitEntry.TABLE_NAME,projection,selection,selectionArgs,null,null,sortOrder);
                break;
            case HABIT_ID :
                selection = HabitEntry._ID + "=?";
                selectionArgs = new String[]{String.valueOf(ContentUris.parseId(uri))};
                cursor = sqLiteDatabase.query(HabitEntry.TABLE_NAME,projection,selection,selectionArgs,null,null,sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Cannot query Un Known Uri" + uri);
        }
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        int match = uriMatcher.match(uri);
        switch (match){
            case HABITS :
                return HabitEntry.CONTENT_LIST_TYPE;
            case HABIT_ID :
                return HabitEntry.CONTENT_ITEM_TYPE;
            default:
                throw new IllegalArgumentException("Un Known Uri : " + uri + "With Match" + match);
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        SQLiteDatabase sqLiteDatabase = habitDbHelper.getWritableDatabase();

        long id = sqLiteDatabase.insert(HabitEntry.TABLE_NAME,null,values);

        return ContentUris.withAppendedId(uri,id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
